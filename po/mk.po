# Macedonian translation for MATE Tweak
# Copyright (c) 2010 Rosetta Contributors and Canonical Ltd 2010
# This file is distributed under the same license as the package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2010.
#
msgid ""
msgstr ""
"Project-Id-Version: mate-tweak\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2015-07-01 15:28+0100\n"
"PO-Revision-Date: 2010-05-04 11:39+0000\n"
"Last-Translator: Dejan Noveski <Unknown>\n"
"Language-Team: Macedonian <mk@li.org>\n"
"Language: mk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2015-02-25 10:21+0000\n"
"X-Generator: Launchpad (build 17355)\n"

#: ../mate-tweak:408
msgid "GNOME2"
msgstr ""

#: ../mate-tweak:411
msgid "Ubuntu MATE"
msgstr ""

#: ../mate-tweak:415
msgid "Ubuntu MATE with MATE Menu"
msgstr ""

#: ../mate-tweak:419
msgid "Ubuntu MATE with Indicators"
msgstr ""

#: ../mate-tweak:424
msgid "Ubuntu MATE with Indicators and MATE Menu"
msgstr ""

#: ../mate-tweak:427
msgid "Redmond"
msgstr ""

#: ../mate-tweak:431
msgid "Redmond with MATE Menu"
msgstr ""

#: ../mate-tweak:435
msgid "Redmond with Indicators"
msgstr ""

#: ../mate-tweak:440
msgid "Redmond with Indicators and MATE Menu"
msgstr ""

#: ../mate-tweak:444
msgid "Netbook"
msgstr ""

#: ../mate-tweak:448
msgid "Netbook with MATE Menu"
msgstr ""

#: ../mate-tweak:452
msgid "Netbook with Indicators"
msgstr ""

#: ../mate-tweak:457
msgid "Netbook with Indicators and MATE Menu"
msgstr ""

#: ../mate-tweak:461
msgid "Cupertino"
msgstr ""

#: ../mate-tweak:465
msgid "Cupertino with MATE Menu"
msgstr ""

#: ../mate-tweak:469
msgid "Cupertino with Indicators"
msgstr ""

#: ../mate-tweak:474
msgid "Cupertino with Indicators and MATE Menu"
msgstr ""

#: ../mate-tweak:478
msgid "Linux Mint"
msgstr ""

#: ../mate-tweak:482
msgid "openSUSE"
msgstr ""

#: ../mate-tweak:525
msgid "Desktop"
msgstr "Работна површина"

#: ../mate-tweak:526
msgid "Windows"
msgstr "Прозорци"

#: ../mate-tweak:527
msgid "Interface"
msgstr "Интерфејс"

#: ../mate-tweak:578
#, fuzzy
msgid "MATE Tweak"
msgstr "Работна површина"

#: ../mate-tweak:581
msgid "Desktop icons"
msgstr "Икони на работната површина"

#: ../mate-tweak:582
msgid "Performance"
msgstr "Перформанси"

#: ../mate-tweak:583
msgid "Window Behaviour"
msgstr ""

#: ../mate-tweak:584
msgid "Maximus"
msgstr ""

#: ../mate-tweak:585
msgid "Appearance"
msgstr "Изглед"

#: ../mate-tweak:586
msgid "Icons"
msgstr "Икони"

#: ../mate-tweak:587
msgid "Context menus"
msgstr "Контекстни менија"

#: ../mate-tweak:588
msgid "Toolbars"
msgstr "Ленти со алатки"

#: ../mate-tweak:589
msgid "Window manager"
msgstr ""

#: ../mate-tweak:591
msgid "Select the items you want to see on the desktop:"
msgstr "Одберете ги ставките што сакате да се појават на работната површина"

#: ../mate-tweak:592
msgid "Window behaviour and new window placement."
msgstr ""

#: ../mate-tweak:594
msgid "Computer"
msgstr "Компјутер"

#: ../mate-tweak:595
msgid "Home"
msgstr "Дома"

#: ../mate-tweak:596
msgid "Network"
msgstr "Мрежа"

#: ../mate-tweak:597
msgid "Trash"
msgstr "Корпа"

#: ../mate-tweak:598
msgid "Mounted Volumes"
msgstr "Монтирани податочни единици"

#: ../mate-tweak:600
msgid "Don't show window content while dragging them"
msgstr "Не ја прикажувај содржината на прозорците додека се влечат"

#: ../mate-tweak:601
msgid "Use compositing"
msgstr ""

#: ../mate-tweak:603
msgid "Undecorate maximized windows"
msgstr ""

#: ../mate-tweak:604
msgid "Do not auto-maximize new windows"
msgstr ""

#: ../mate-tweak:606
msgid "Window control placement."
msgstr ""

#: ../mate-tweak:608
msgid "Show icons on menus"
msgstr "Прикажи икони на менијата"

#: ../mate-tweak:609
msgid "Show icons on buttons"
msgstr "Прикажи икони на копчињата"

#: ../mate-tweak:610
msgid "Show Input Methods menu in context menus"
msgstr "Прикажи го менито со методите за внесување во контекстните менија"

#: ../mate-tweak:611
msgid "Show Unicode Control Character menu in context menus"
msgstr "Прикажи го менито со уникод контролни карактери во контекстните менија"

#: ../mate-tweak:613
msgid "Style:"
msgstr ""

#: ../mate-tweak:614
msgid "Icon size:"
msgstr "Големина на икони:"

#: ../mate-tweak:624
msgid "Center"
msgstr ""

#: ../mate-tweak:625
msgid "Origin"
msgstr ""

#: ../mate-tweak:626
msgid "Random"
msgstr ""

#: ../mate-tweak:627
msgid "Smart"
msgstr ""

#: ../mate-tweak:656
msgid "Small"
msgstr "Мали"

#: ../mate-tweak:657
msgid "Large"
msgstr "Големи"

#: ../mate-tweak:663
#, fuzzy
msgid "Traditional (Right)"
msgstr "Традиционално (Десно)"

#: ../mate-tweak:664
msgid "Contemporary (Left)"
msgstr ""

#: ../mate-tweak:672
msgid "Marco (Simple desktop effects)"
msgstr ""

#: ../mate-tweak:674
msgid "Metacity (Simple desktop effects)"
msgstr ""

#: ../mate-tweak:676
msgid "Mutter (Elegant GPU accelerated desktop effects)"
msgstr ""

#: ../mate-tweak:678
msgid "Compiz (Advanced GPU accelerated desktop effects)"
msgstr ""

#: ../mate-tweak:681
msgid "The new window manager will be activated upon selection."
msgstr ""

#: ../mate-tweak:682
msgid "Select a window manager."
msgstr ""

#: ../mate-tweak:687
msgid "Select a panel layout to change the user interface."
msgstr ""

#: ../mate-tweak:688
msgid ""
"The new panel layout will be activated on selection and destroy any "
"customisations you might have made."
msgstr ""

#: ../mate-tweak:693
msgid "Text below items"
msgstr "Текст под ставките"

#: ../mate-tweak:694
msgid "Text beside items"
msgstr "Текст до ставките"

#: ../mate-tweak:695
msgid "Icons only"
msgstr "Само икони"

#: ../mate-tweak:696
msgid "Text only"
msgstr "Само текст"

#~ msgid "Buttons layout:"
#~ msgstr "Распоред на копчиња"

#~ msgid "Buttons labels:"
#~ msgstr "Лабели на копчиња:"

#~ msgid "Use system font in titlebar"
#~ msgstr "Користи системски фонт во насловната лента"

#~ msgid "Mac style (Left)"
#~ msgstr "Mac стил (Лево)"

#~ msgid "Desktop Settings"
#~ msgstr "Поставувања на работната површина"

#~ msgid "Desktop Configuration Tool"
#~ msgstr "Алатка за конфигурирање на работната површина"

#~ msgid "Desktop Items"
#~ msgstr "Предмети на работната површина"

#~ msgid "Gnome Settings"
#~ msgstr "Поставки за Gnome"

#~ msgid "Fine-tune Gnome settings"
#~ msgstr "Фино подесување на Gnome"
